# statify

[![pipeline status](https://gitlab.com/avalonparton/statify/badges/master/pipeline.svg)](https://gitlab.com/avalonparton/statify/commits/master)

Spotify top tracks web app - [statify.herokuapp.com](https://statify.herokuapp.com/)

Main page
![screenshot](https://i.imgur.com/MXhMhzu.png)

Stats page
![screenshot2](https://i.imgur.com/mccuql9.png)

## Running

1. Install pre-reqs

    ```bash
    pip3 install -r requirements.txt
    ```

1. Register your app in the [Spotify Developer Dashboard](https://developer.spotify.com/my-applications/#!/applications)

1. Add your app's redirect url to the spotify app

1. Export your app's ID and secret

    ```bash
    export SPOTIFY_CLIENT_ID='your-spotify-client-id'
    export SPOTIFY_CLIENT_SECRET='your-spotify-client-secret'
    export SPOTIFY_REDIRECT_URI='https://statify.herokuapp.com/spotify/login' # change this
    ```

1. Run the flask application

    ```bash
    PORT=5000 python3 app/app.py
    ```

## Reference

* [Flask](http://flask.pocoo.org/docs/1.0/quickstart/#quickstarthttp://flask.pocoo.org/docs/1.0/quickstart/#quickstart)
* [stupid.css](https://mdipierro.github.io/stupid.css/index.html)
* [Spotify API (beta)](https://developer.spotify.com/documentation/web-api/reference-beta/)
