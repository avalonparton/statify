#!/usr/local/bin/python3
from flask import Flask, send_from_directory, redirect, render_template, request, url_for
from urllib.parse import urlencode
import os
import spotify
app = Flask(__name__)

# Get app IP and port from env vars, default to http://localhost:5000
APP_IP = os.environ.get('APP_IP', "127.0.0.1")
PORT = os.environ.get("PORT", 5000)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static', 'images'),
                               'favicon.ico', mimetype='image/x-icon')


@app.route('/spotify')
def spotify_login():
    """ Redirects user to spotify to be prompted for login.
    The user is then redirected to /spotify/login after logging in
    """
    redirect_uri = APP_IP + url_for('spotify_login_success')
    url = spotify.get_login_url(redirect_uri=redirect_uri)
    return redirect(url)


@app.route('/spotify/login')
def spotify_login_success():
    """ Converts the response code to an auth token.
    Redirect_uri must be the same as the one used when asking for token.
    User is then redirected to /stats
    """
    code = request.args.get('code')
    redirect_uri = APP_IP + url_for('spotify_login_success')
    token = spotify.auth_code_to_token(redirect_uri, code)
    return redirect(url_for('stats', token=token))


@app.route('/stats')
def stats():
    print("args:", request.args)
    token = request.args.get('token')
    if token is None:
        return redirect(url_for('spotify_login'))
    track_limit = request.args.get('track_limit', 50)
    artist_limit = request.args.get('artist_limit', 20)
    time_range = request.args.get('time_range', 'medium_term')
    tracks = spotify.get_users_top(auth_token=token, endpoint="tracks", limit=track_limit, offset=0, time_range=time_range)
    for track in tracks['items']:
        print(track)
    artists = spotify.get_users_top(auth_token=token, endpoint="artists", limit=artist_limit, offset=0, time_range=time_range)

    if tracks.get('error') or artists.get('error'):
        if tracks.get('error').get('message') == 'The access token expired':
            print("Token expired, redirecting user to login page")
            return redirect(url_for('spotify_login'))

    return render_template('stats.html', tracks=tracks, artists=artists, view="default")


@app.route("/index")
@app.route("/")
def index():
    return render_template('index.html')


if __name__ == "__main__":
    #app.debug = True
    app.host = "0.0.0.0"
    app.port = PORT
    app.run()
