""" Spotify API commands """
from base64 import b64encode
import json
import requests
import os

CLIENT_ID = os.environ['SPOTIFY_CLIENT_ID']
CLIENT_SECRET = os.environ['SPOTIFY_CLIENT_SECRET']


def auth_code_to_token(redirect_uri, code):
    """ Asks Spotify for an auth token from a code

    Args:
        redirect_uri:  Redirect URI
        code:          Authorization code

    Returns:
        Access token
    """
    payload = {
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": redirect_uri
    }
    base64encoded = b64encode(("{}:{}".format(CLIENT_ID, CLIENT_SECRET)).encode())
    headers = {"Authorization": f"Basic {base64encoded.decode()}"}
    url = "https://accounts.spotify.com/api/token"
    req = requests.post(url, data=payload, headers=headers)
    data = json.loads(req.text)
    return data["access_token"]


def get_users_top(auth_token, endpoint, limit=20, offset=0, time_range="medium_term"):
    """ Returns the JSON response from Spotify's User Top Tracks endpoint
    https://developer.spotify.com/documentation/web-api/reference/personalization/get-users-top-artists-and-tracks/

    Args:
        auth_token: User's authorization token
        endpoint:   Either "artists" or "tracks"
        limit:      Number of songs to return. Max 50, default 20
        offset:     Where to start (use this to get next pages)
        time_range: One of the following:
            "long_term" (calculated from several years of data)
            "medium_term" (approximately last 6 months)
            "short_term" (approximately last 4 weeks)

    """
    url = f"https://api.spotify.com/v1/me/top/{endpoint}?limit={limit}&offset={offset}&time_range={time_range}"
    headers = {"Authorization": f"Bearer {auth_token}"}
    req = requests.get(url, headers=headers)
    data = json.loads(req.text)
    return data


def get_login_url(redirect_uri):
    """ Returns the /authorize url for this app """
    scope = "playlist-modify-private user-read-recently-played user-top-read"
    params = {
        "client_id": CLIENT_ID,
        "response_type": "code",
        "redirect_uri": redirect_uri,
        "scope": scope,
        "show-dialog": True
    }

    query = "&".join("{}={}".format(*i) for i in params.items())

    return f"https://accounts.spotify.com/authorize/?{query}"


def create_user_playlist(auth_token, user_id, playlist_name):
    """ Sends a POST to the Spotify Create a Playlist endpoint
    https://developer.spotify.com/documentation/web-api/reference/playlists/create-playlist/

    Args:
        auth_token:     User's authorization token
        user_id:        User's Spotify account ID
        playlist_name:  Name of the playlist

    """
    url = f"https://api.spotify.com/v1/users/{user_id}/playlists"
